package presenter;

import Interface.MainInterface;
import model.Array;


public class Presenter implements MainInterface.presenter {
    private Array arr;
    private MainInterface.view view;


    public Presenter(MainInterface.view view) {
        this.arr = new Array();
        this.view = view;
    }

    @Override
    public void addElement(int element, int index) {
        arr.addElement(element,index);
        view.message(arr.StringArrayToString());
    }

    @Override
    public void addArray(int[] arr1) {
        arr.addArray(arr1);
        view.message(arr.StringArrayToString());
    }

    @Override
    public void addStart(int element) {
        arr.addStart(element);
        view.message(arr.StringArrayToString());
    }

    @Override
    public void addEnd(int element) {
        arr.addEnd(element);
        view.message(arr.StringArrayToString());
    }

    @Override
    public void deleteElement(int element) {
        arr.deleteElement(element);
        view.message(arr.StringArrayToString());
    }

    @Override
    public void deleteAll() {
        arr.deleteAll();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void deleteStart() {
        arr.deleteStart();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void deleteEnd() {
        arr.deleteEnd();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void sortMaxMin() {
        arr.sortMaxMin();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void sortMinMax() {
        arr.sortMinMax();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void reverse() {
        arr.reverse();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void halfReverse() {
        arr.halfReverse();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void StringArrayToString() {
        arr.StringArrayToString();
        view.message(arr.StringArrayToString());
    }

    @Override
    public void print() {
        arr.print();
        view.message(arr.StringArrayToString());
    }
}
