package view;

import Interface.MainInterface;
import presenter.Presenter;

public class View implements MainInterface.view {
    public static void main(String[] args) {
        MainInterface.view view = new View();
        MainInterface.presenter presenter = new Presenter(view);
        presenter.addElement(1, 3);
        presenter.addEnd(2);
    }

    @Override
        public void message(Object object){
            System.out.println(object.toString());

    }
}
