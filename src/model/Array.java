package model;

import Interface.MainInterface;

import java.util.Arrays;

public class Array implements MainInterface.model {
    private int[] arr = new int [10];

    @Override
    public void addElement(int element, int index) {
        if (index < 0) {
            System.out.println("Error");
        } else {
            int[] temp = new int[arr.length + 1];
            for (int i = 0; i != arr.length; i++) {
                if (i == index) {
                    temp[i] = element;
                } else {
                    temp[i] = arr[i];
                }
            }
            arr = temp;
        }
    }


    @Override
    public void addArray(int[] arr1) {
        int[] temp = new int[arr.length + arr1.length];
        for (int i = 0; i != arr.length; i++) {
            temp[i] = arr[i];
        }
        for (int i = 0; i != arr1.length; i++) {
            temp[arr.length + i] = arr1[i];
        }
        arr = temp;
    }

    @Override
    public void addStart(int element) {
        int[] temp = new int[arr.length + 1];
        temp[0] = element;
        for (int i = 0; i != arr.length; i++) {
            temp[i + 1] = arr[i];
        }
        arr = temp;
    }

    @Override
    public void addEnd(int element) {
        int[] temp = new int[arr.length + 1];
        temp[temp.length - 1] = element;
        for (int i = 0; i != arr.length; i++) {
            temp[i] = arr[i];
        }
        arr = temp;
    }

    @Override
    public void deleteElement(int index) {
        if (index >= arr.length || index < 0) {
            System.out.println("Error");
        } else {
            int[] temp = new int[arr.length - 1];
            for (int i = 0; i != index; i++) {
                temp[i] = arr[i];
            }
            for (int i = index + 1; i != arr.length; i++) {
                temp[i - 1] = arr[i];
            }
            arr = temp;
        }
    }

    @Override
    public void deleteAll() {
        int[] temp = new int[0];
        arr = temp;
    }

    @Override
    public void deleteStart() {
        if (arr.length == 0) {
            System.out.println("Error");
            System.out.println(getSize());
        } else {
            int[] temp = new int[arr.length - 1];
            for (int i = 1; i < arr.length; i++) {
                temp[i - 1] = arr[i];
            }
            arr = temp;
        }
    }

    @Override
    public void deleteEnd() {
        if (arr.length == 0) {
            System.out.println("Error");
            System.out.println(getSize());
        } else {
            int[] temp = new int[arr.length - 1];
            for (int i = 0; i < arr.length - 1; i++) {
                temp[i] = arr[i];
            }
            arr = temp;
        }
    }

    @Override
    public void sortMaxMin() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
            System.out.println(getSize());
        } else {
            for (int i = 0; i != arr.length; i++) {
                for (int j = 0; j != arr.length - 1; j++) {
                    if (arr[j + 1] > arr[j]) {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }
    }

    @Override
    public void sortMinMax() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
            System.out.println(getSize());
        } else {
            for (int i = 0; i != arr.length; i++) {
                for (int j = 0; j != arr.length - 1; j++) {
                    if (arr[j + 1] < arr[j]) {
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }
    }

    @Override
    public void reverse() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
            System.out.println(getSize());
        } else {
            for (int i = 0; i != arr.length / 2; i++) {
                int temp = arr[i];
                arr[i] = arr[arr.length - i - 1];
                arr[arr.length - i - 1] = temp;
            }
        }
    }

    @Override
    public void halfReverse() {
        if (arr.length == 0 || arr.length == 1) {
            System.out.println(Arrays.toString(arr));
            System.out.println(getSize());
        } else {
            for (int i = 0; i != (arr.length / 2); i++) {
                int temp = arr[i];
                arr[i] = arr[arr.length / 2 + i];
                arr[arr.length / 2 + i] = temp;
            }
        }
    }

    @Override
    public String StringArrayToString() {
        return Arrays.toString(arr);
    }

    @Override
    public void print() {
        for (int i = 0; i != arr.length; i++) {
            System.out.print(arr[i] + "; ");
        }
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int getSize() {
        return arr.length;
    }
}
