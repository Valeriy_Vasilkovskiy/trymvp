package Interface;

public interface MainInterface {
    interface model{
        void addElement(int element, int index);

        void addArray(int[] arr1);

        void addStart(int element);

        void addEnd(int element);

        void deleteElement(int element);

        void deleteAll();

        void deleteStart();

        void deleteEnd();

        void sortMaxMin();

        void sortMinMax();

        void reverse();

        void halfReverse();

        String StringArrayToString();

        void print();
    }
    interface presenter{
        void addElement(int element, int index);

        void addArray(int[] arr1);

        void addStart(int element);

        void addEnd(int element);

        void deleteElement(int element);

        void deleteAll();

        void deleteStart();

        void deleteEnd();

        void sortMaxMin();

        void sortMinMax();

        void reverse();

        void halfReverse();

        void StringArrayToString();

        void print();
    }
    interface view {
        void message(Object object);
    }
}
